import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./paginas/inicio-sesion/inicio-sesion.module').then( m => m.InicioSesionPageModule) },
  { path: 'ideas', loadChildren: './paginas/ideas/ideas.module#IdeasPageModule'},
  { path: 'idea', loadChildren: './paginas/ideas/idea/idea.module#IdeaPageModule' },
  { path: 'crear-idea', loadChildren: './paginas/ideas/crear-idea/crear-idea.module#CrearIdeaPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
