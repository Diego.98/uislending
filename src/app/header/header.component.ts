import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { servicios } from "../servicios.service";
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  private saldo: any;
  constructor(private router: Router, public actionSheetController: ActionSheetController,  private servicios: servicios) { }

  ngOnInit() {
    this.saldo = localStorage.getItem("saldo");
  }
  async opciones() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      buttons: [{
        text: 'Cerrar sesión',
        icon: 'exit',
        handler: () => {
          this.servicios.cerrarSesion().subscribe((data)=>{
            localStorage.setItem("idUsuario","");
            this.router.navigate(['/home']);
          }, err =>{
            localStorage.setItem("idUsuario","");
            this.router.navigate(['/home']);
            throw err;
          });
        }
      }]
    });
    await actionSheet.present();
  }
}
