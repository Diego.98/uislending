import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Router } from '@angular/router';
import { catchError, map, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AlertController } from '@ionic/angular';
import { environment } from '../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class servicios {
  constructor(public alertController: AlertController, private http: HttpClient, private router: Router) { }
  iniciarSesion(correo, contrasena) {
    var datos = {
      username: correo,
      password: contrasena
    };
    return this.http.post(`${environment.urlBack}api/auth/token/login/`, datos);
  }

  cerrarSesion() {
    if (this.verificar_sesion()) {
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token '+localStorage.getItem("token")
      });
      let options = { headers: headers };
      return this.http.post(`${environment.urlBack}api/auth/token/logout`, null, options);
    }
  }

  verificar_sesion() {
    if (localStorage.getItem('imagen') && localStorage.getItem('token') && localStorage.getItem('idUsuario') && localStorage.getItem('token') != "" && localStorage.getItem('idUsuario') != "" && localStorage.getItem('imagen') != "") {
      return true;
    } else {
      return false;
    }
  }

  invertir(monto_invertido, idUsuario, idIdea) {
    if (this.verificar_sesion()) {
      var datos = {
        monto_invertido: monto_invertido,
        monto_interese: "1",  //esto hay que quitarlo
        usuario: idUsuario,
        idea: idIdea,
      };
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token '+localStorage.getItem("token")
      });
      let options = { headers: headers };
      return this.http.post(`${environment.urlBack}api/inversiones/`, datos, options);
    } else {
      this.presentAlertFallo();
      this.router.navigate(['/home']);
    }
  }
  async presentAlertFallo() {
    const alert = await this.alertController.create({
      message: 'Parece que no estas logueado, inicia sesión primero.',
      buttons: ['OK']
    });
    await alert.present();
  }
  crear_idea(idea) {
    if (this.verificar_sesion()) {
      let headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token '+localStorage.getItem("token")
      });
      let options = { headers: headers };
      var datos = {
        nombre: idea.nombre,
        descripcion: idea.descripcion,
        monto_objetivo: idea.monto_objetivo,
        intereses: idea.intereses,
        fecha_limite: idea.fecha_limite,
        fecha_reembolso: idea.fecha_reembolso,
        categoria: idea.categoria,
        imagen: idea.imagen
      };
      return this.http.post(`${environment.urlBack}api/ideas/`, datos,options);

    } else {
      this.presentAlertFallo();
      // this.router.navigate(['iniciar-sesion']);
    }
  }









  async presentAlertFalloLogueo() {
    const alert = await this.alertController.create({
      message: 'Parece que no estas logueado, inicia sesión primero.',
      buttons: ['OK']
    });
    await alert.present();

  }

  async presentAlertFalloUsuario() {
    const alert = await this.alertController.create({
      message: 'El usuario que ingresaste no existe o la contraseña no coincide, por favor intenta de nuevo',
      buttons: ['OK']
    });
    await alert.present();
  }
}
