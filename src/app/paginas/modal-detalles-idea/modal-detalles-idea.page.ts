import { Component, OnInit, Input } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { servicios } from "../../servicios.service";
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-detalles-idea',
  templateUrl: './modal-detalles-idea.page.html',
  styleUrls: ['./modal-detalles-idea.page.scss'],
})
export class ModalDetallesIdeaPage implements OnInit {
  @Input() public id: string;
  @Input() public nombre: string;
  @Input() public descripcion: string;
  @Input() public monto_objetivo: number;
  @Input() public fecha_limite: string;
  @Input() public fecha_publicada: string;
  @Input() public fecha_reembolso: string;
  @Input() public monto_actual: number;
  @Input() public intereses: number;
  @Input() public estado: number;
  @Input() public imagen: number;
  @Input() public usuarioIdea: number;
  private montoAInvertir;
  private diasFaltantes;
  private porcentajeProgreso;
  private usuarioLogueado:any = localStorage.getItem("idUsuario");
  respuestaInversion: Observable<any>;
  constructor(private router: Router, public alertController: AlertController, public modalController: ModalController, private servicios: servicios) {
    
  }

  ngOnInit() {
    this.convertirParametros();
  }
  convertirParametros() {
    var fechaHoy = new Date().getTime();
    var fechaServidor = new Date(this.fecha_limite).getTime() + 5 * 1000 * 60 * 60;
    this.diasFaltantes = Math.ceil((fechaServidor - fechaHoy) / (1000 * 3600 * 24));
    this.porcentajeProgreso = (this.monto_actual) / this.monto_objetivo;
  }

  cerrarModal() {
    this.modalController.dismiss({
      'dismissed': true,
      'montoActual': this.monto_actual
    });
  }
  async presentAlertExito() {
    const alert = await this.alertController.create({
      message: 'Se ha invertido correctamente en la idea.',
      buttons: ['OK']
    });
    await alert.present();
  }
  async presentAlertFallo() {
    const alert = await this.alertController.create({
      message: 'No has podido inverir en la idea, intenta mas tarde.',
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentAlertFalloMonto() {
    const alert = await this.alertController.create({
      message: 'El monto que ingresaste no es valido, ingresa un valor mayor a 1000.',
      buttons: ['OK']
    });
    await alert.present();
  }
  invertir() {
    if (this.servicios.verificar_sesion()) {
      if (this.montoAInvertir > 0) {
        this.servicios.invertir(this.montoAInvertir, localStorage.getItem("idUsuario"), this.id).subscribe(data => {
          if (data) {
            this.monto_actual += this.montoAInvertir;
            this.presentAlertExito();
            this.cerrarModal();
          } else {
            this.presentAlertFallo();
          }
        }, error => {
          this.presentAlertFallo();
        });
      } else {
        this.presentAlertFalloMonto();
      }
    } else {
      this.servicios.presentAlertFalloLogueo();
      this.router.navigate(['/home']);
      this.cerrarModal();
    }

  }

  // cerrarSesion(){
  //   this.servicios.cerrarSesion().subscribe(data => {
  //     console.log(data);
  //   });
  //   }
}
