import { Component} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { servicios } from "../../servicios.service";
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-inicio-sesion',
  templateUrl: './inicio-sesion.page.html',
  styleUrls: ['./inicio-sesion.page.scss'],
})
export class InicioSesionPage {
  private subscription: Subscription;
  private correo;
  private contrasena;
  private loading:any;
  constructor(public loadingController: LoadingController, public alertController: AlertController, private servicios: servicios, private router: Router) {
  }
  ionViewDidEnter(){
    localStorage.clear();
  }
  
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Iniciando sesión...',
    });
    await this.loading.present();
  }
  async eliminarLoading(){
    await this.loading.dismiss();
  }
  mostrar(){
    let e = document.getElementById("eye");
    let tipo = e.getAttribute("type");
    (tipo == 'password') ? e.setAttribute("type","text") : e.setAttribute("type","password");
  }
  iniciarSesion() {
    this.presentLoading().then(()=>{
      this.subscription=this.servicios.iniciarSesion(this.correo, this.contrasena).subscribe((res) => {
        console.log(res);
        localStorage.setItem('token', res["token"]);
        localStorage.setItem('idUsuario', res["id"]);  
        localStorage.setItem('imagen', res["imagen"]);
        localStorage.setItem('saldo', res["saldo"]);
        if(this.servicios.verificar_sesion()){
          this.router.navigate(['ideas']).then(()=>{
            this.eliminarLoading();
          });
        }else{
          this.router.navigate(['home']).then(()=>{
            this.eliminarLoading();
          });
        }
      },
      (err) => {
        this.servicios.presentAlertFalloUsuario().then(()=>{
          this.eliminarLoading();
        });
      });
    });
  }

  ionViewDidLeave() {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }
  ngOnDestroy(){
    console.log("me destruí");
  }
}
