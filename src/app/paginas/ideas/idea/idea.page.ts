import { Component, OnInit, Input, NgZone } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalDetallesIdeaPage } from '../../modal-detalles-idea/modal-detalles-idea.page'
@Component({
  selector: 'app-idea',
  templateUrl: './idea.page.html',
  styleUrls: ['./idea.page.scss'],
})
export class IdeaPage implements OnInit {
  @Input() public id: string;
  @Input() public nombre: string;
  @Input() public descripcion: string;
  @Input() public monto_objetivo: number;
  @Input() public fecha_limite: string;
  @Input() public fecha_publicada: string;
  @Input() public fecha_reembolso: string;
  @Input() public monto_actual: number;
  @Input() public intereses: number;
  @Input() public estado: number;
  @Input() public imagen: number;
  @Input() public imagenUsuario: number;
  @Input() public usuario: number;
  private diasFaltantes;
  private porcentajeProgreso;
  constructor(public modalController: ModalController, private zone: NgZone) {
  }

  ngOnInit() {
    this.convertirParametros();
  }
  convertirParametros() {
    var fechaHoy = new Date().getTime();
    var fechaServidor = new Date(this.fecha_limite).getTime() + 5 * 1000 * 60 * 60;
    this.diasFaltantes = Math.ceil((fechaServidor - fechaHoy) / (1000 * 3600 * 24));
    this.porcentajeProgreso = (this.monto_actual) / this.monto_objetivo;
  }


  async presentModal() {
    console.log(this.usuario, "logueado");
    const modal = await this.modalController.create({
      component: ModalDetallesIdeaPage,
      componentProps: {
        'id': this.id,
        'nombre': this.nombre,
        'descripcion': this.descripcion,
        'monto_objetivo': this.monto_objetivo,
        'fecha_limite': this.fecha_limite,
        'fecha_publicada': this.fecha_publicada,
        'fecha_reembolso': this.fecha_reembolso,
        'monto_actual': this.monto_actual,
        'intereses': this.intereses,
        'estado': this.estado,
        'imagen': this.imagen,
        'imagenUsuario': this.imagenUsuario,
        'usuarioIdea': this.usuario
      }
    });
    modal.onDidDismiss().then((data: any) => {
      this.zone.run(() => {
        this.monto_actual = data.data.montoActual;
      });
    });
    return await modal.present();
  }
}
