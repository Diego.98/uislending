import { NgModule, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { IdeaPage } from './idea.page';

const routes: Routes = [
  {
    path: '',
    component: IdeaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    IdeaPage,
    IdeaPageModule
  ],
  declarations: [IdeaPage]
})
export class IdeaPageModule {
  
}
