import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearIdeaPage } from './crear-idea.page';

describe('CrearIdeaPage', () => {
  let component: CrearIdeaPage;
  let fixture: ComponentFixture<CrearIdeaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearIdeaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearIdeaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
