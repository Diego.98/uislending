import { Component, OnInit, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { servicios } from "../../../servicios.service";
import { Subscription } from "rxjs";
import { AlertController, LoadingController } from '@ionic/angular';
import { error } from '@angular/compiler/src/util';
import { environment } from '../../../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-idea',
  templateUrl: './crear-idea.page.html',
  styleUrls: ['./crear-idea.page.scss'],
})
export class CrearIdeaPage {
  private suscripcionCrearIdea: Subscription;
  private subscripcionCategoria: Subscription;
  private loading:any;
  base64String: string | ArrayBuffer;
  imageSrc: string | ArrayBuffer;
  idea = {};
  categorias = [];
  categoriasTraidas: Observable<any>;
  constructor(public loadingController: LoadingController, public router: Router,public alertController: AlertController, private servicios: servicios, public getCategoriasDjango: HttpClient, private zone: NgZone) {
    this.getCategorias();
  }

  ionViewDidEnter() {
  }

  readURL(event: any): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = e => {
        this.imageSrc = reader.result;
        this.base64String = reader.result;
      };
      reader.readAsDataURL(file);
    }
  }
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Publicando idea...',
    });
    await this.loading.present();
  }
  async eliminarLoading(){
    await this.loading.dismiss();
  }
  getCategorias() {
    this.categoriasTraidas = this.getCategoriasDjango.get(`${environment.urlBack}api/categorias/`);
    this.subscripcionCategoria = this.categoriasTraidas.subscribe(data => {
      this.zone.run(() => {
        this.categorias = data;
      });
    });
  }
  async presentAlertExito() {
    const alert = await this.alertController.create({
      message: 'La idea se ha creado correctamente.',
      buttons: ['OK']
    });
    await alert.present();
  }
  async presentAlertFallo() {
    const alert = await this.alertController.create({
      message: 'La idea no se ha podido crear, intenta mas tarde.',
      buttons: ['OK']
    });
    await alert.present();
  }
  guardarIdea() {
    this.presentLoading();
    this.idea["imagen"] = String(this.base64String);
    this.idea["categoria"] = parseInt(this.idea["categoria"]);
    this.idea["usuario"] = localStorage.getItem('idUsuario');
    this.idea["fecha_limite"] = this.idea["fecha_limite"].split("T")[0];
    this.idea["fecha_reembolso"] = this.idea["fecha_reembolso"].split("T")[0];
    this.suscripcionCrearIdea = this.servicios.crear_idea(this.idea).subscribe(data => {
      if (data) {
        this.presentAlertExito();
        this.router.navigate(['/ideas']).then(()=>{
          this.eliminarLoading();
        });
      } else {
        this.presentAlertFallo().then(()=>{
          this.eliminarLoading();
        });
      }
    }, error => {
      this.presentAlertFallo().then(()=>{
        this.eliminarLoading();
      });
    });
  }
  ionViewDidLeave() {
    if (this.suscripcionCrearIdea) {
      this.suscripcionCrearIdea.unsubscribe();
    }
    this.subscripcionCategoria.unsubscribe();
  }
}
