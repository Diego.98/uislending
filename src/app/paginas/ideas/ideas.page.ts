import { Component, OnInit, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { servicios } from "../../servicios.service";
import { Router } from '@angular/router';
import { ignoreElements } from 'rxjs/operators';
import { enableDebugTools } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-ideas',
  templateUrl: './ideas.page.html',
  styleUrls: ['./ideas.page.scss'],
})
export class IdeasPage {
  private paginasIdeasCargadas = 1;
  ideas = [];
  private subscription: Subscription;
  private imagen = localStorage.getItem('imagen');
  ideasTraidas: Observable<any>;
  private logueado: String = localStorage.getItem("idUsuario");
  constructor(private router: Router, private servicios: servicios, public getIdeasDjango: HttpClient, private zone: NgZone) {

  }

  ionViewDidEnter() {
    this.paginasIdeasCargadas=1;
    this.imagen = localStorage.getItem('imagen');
    this.logueado = localStorage.getItem("idUsuario");
    if (!this.servicios.verificar_sesion()) {
      this.router.navigate(['/home']);
    }
    this.getIdeasPrimeraVez();
  }
  getIdeasPrimeraVez() {
    this.ideasTraidas = this.getIdeasDjango.get(`${environment.urlBack}api/ideas/?page=1`);
    this.subscription = this.ideasTraidas.subscribe(data => {
      this.zone.run(() => {
        this.ideas = data.results;
        console.log(this.ideas);
      });
    }, err => {
      throw err;
    });
  }
  getIdeasPrimeraVez2(event:any) {
    this.ideasTraidas = this.getIdeasDjango.get(`${environment.urlBack}api/ideas/?page=1`);
    this.subscription = this.ideasTraidas.subscribe(data => {
      this.zone.run(() => {
        this.ideas = data.results;
        event.target.complete();
      });
    }, err => {
      throw err;
    });
  }

  getIdeas(infiniteScroll) {
    this.paginasIdeasCargadas++;
    this.ideasTraidas = this.getIdeasDjango.get(`${environment.urlBack}api/ideas/?page=${this.paginasIdeasCargadas}`) || null;
    this.subscription = this.ideasTraidas.subscribe(data => {
      this.zone.run(() => {
        this.ideas=this.ideas.concat(data.results);
        infiniteScroll.target.complete(); 
      });
    }, err => {
      this.zone.run(() => {
        this.paginasIdeasCargadas--;
        infiniteScroll.target.complete(); 
      });
    });
  }
  ionViewDidLeave() {
    this.subscription.unsubscribe();
  }

  doInfinite(infiniteScroll) {
    this.getIdeas(infiniteScroll);
  }
}
