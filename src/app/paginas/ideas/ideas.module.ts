import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { IdeasPage } from './ideas.page';
import { IdeaPage } from './idea/idea.page';
import { HeaderComponentModule } from 'src/app/header/header.component.module';

const routes: Routes = [
  {
    path: '',
    component: IdeasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    HeaderComponentModule
  ],
  declarations: [IdeasPage, IdeaPage]
})
export class IdeasPageModule {}
